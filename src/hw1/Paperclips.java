package hw1;

public class Paperclips {

	public static void main(String [] args){
		int nHours = 24, nClips = 22025;
		int minutesPerHour = 60, secondsPerMinute = 60;
		
		System.out.println(runCalc(nHours, nClips, minutesPerHour, secondsPerMinute));
		
		//My (mostly estimated) times
		//6 is the number per minute
		nClips = 6 * minutesPerHour * nHours;
		System.out.println((int)runCalc(nHours, nClips, minutesPerHour, secondsPerMinute));
	}
	
	//Because I don't want to have all this code cluttering my main function.
	private static double runCalc(int hour, int clip, int minute, int seconds){
		double hoursPerClip = (double)hour/clip;
		double minutesPerClip = (double)hoursPerClip * minute;
		double secondsPerClip = (double)minutesPerClip * seconds;
		
		return secondsPerClip;
	}
}