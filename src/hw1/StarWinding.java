package hw1;

public class StarWinding {
	
	public static void main(String [] args){
		int jump = 1;
		
		for(int attempts = 0; attempts <= 11; attempts++){ //attempts makes the program run through each variation on jumps.
			int node = 0;
			for(int i = 0; i <= 11; i++){
				if(jump == 5 || jump == 7){
					System.out.print(node + " ");
				}
				node += jump;
				if(node >= 12){ //Going around the circle
					node -= 12;
				}
			}
			if(jump == 5 || jump == 7){
				System.out.println();
			}
			jump++;
		}
	}
}
