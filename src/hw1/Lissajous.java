package hw1;

public class Lissajous{

	public static void main(String [] args){
		//Loops!
		for(int t = 0; t <= 360; t += 40){
			if(t == 360){
				System.out.println("(" + x(t) + "," + y(t) + ")");
			}else{
				System.out.println("(" + x(t) + "," + y(t) + "),");
			}
			
		}
	}
	
	private static double x(int t){
		double x = (double)(4 * Math.sin(Math.toRadians(t))); //Never forget to change to radians for trig calculations.
		return x;
	}
	
	private static double y(int t){
		double y = (double)(Math.sin(2 * Math.toRadians(t)));
		return y;
	}
}
