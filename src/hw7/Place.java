package hw7;

public class Place {
	
	private String name;
	private String description;
	private LatLon latLon;
	private int cluster;
	
	public Place(String n, String d, LatLon ll){
		this.name = n;
		this.description = d;
		this.latLon = ll;
		this.cluster = 0;
	}
	
	public String getName(){
		return this.name;
	}
	
	public String getDescription(){
		return this.description;
	}
	
	public LatLon getLocation(){
		return this.latLon;
	}
	
	public void setClusterID(int ID){
		this.cluster = ID;
	}
	
	public int getClusterID(){
		return this.cluster;
	}
	
	public String toString(){
		String str = getName() + " " + getLocation().getLatitude() + " " + getLocation().getLongitude() + " " + getClusterID();
		return str;
	}
}
