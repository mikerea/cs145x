package hw7;

import java.util.ArrayList;

public class Cluster {
	
	private ArrayList<LatLon> latLon;
	
	public Cluster() {
		this.latLon = new ArrayList<LatLon>();
	}
	
	public void add(LatLon ll){
		this.latLon.add(ll);
	}
	
	public LatLon getMeanLocation(){

		double lonTot = 0.0;
		double latTot = 0.0;
		
		for(int i = 0; i < latLon.size(); i++){
			lonTot += latLon.get(i).getLongitude();
			latTot += latLon.get(i).getLatitude();
		}
		
		return new LatLon(latTot/latLon.size(), lonTot/latLon.size());
	}
}
