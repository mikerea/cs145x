package hw7;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {
	public static void main(String[] args){
	}
	
	public static void clusterAndMap(ArrayList<Place> places, int k, File f) throws FileNotFoundException{
		KMeans.cluster(places, k);
		KmlUtilities.write(f, places);
	}
	
	public static void clusterAndMap(File f1, int k, File f2) throws FileNotFoundException {
		ArrayList<Place> place = new ArrayList<Place>();
		Scanner a = new Scanner(f1);
		while (a.hasNext()) {
			
			String[] s = a.nextLine().split("\t");
			String n = s[0];
			String d = s[1];
			
			double lat = Double.parseDouble(s[2]);
			double lon = Double.parseDouble(s[3]);
			
			LatLon loc = new LatLon(lat, lon);
			place.add(new Place(n, d, loc));
		}
		a.close();
		clusterAndMap(place, k, f2);
	}
}
