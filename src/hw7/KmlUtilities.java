package hw7;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;

public class KmlUtilities {
	public static void write(File f, ArrayList<Place> places) throws FileNotFoundException{
		
		PrintWriter print = new PrintWriter(f);
		print.println("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		print.println("<kml xmlns=\"http://www.opengis.net/kml/2.2\">");
		print.println("<Document>");
		print.println("<name>KMap</name>");
		print.println("<Style id=\"cluster0\"><IconStyle>");
		print.println("  <color>ff00ff00</color>");
		print.println("  <Icon><href>http://www.gstatic.com/mapspro/images/stock/503-wht-blank_maps.png</href></Icon>");
		print.println("</IconStyle></Style>");
		print.println("<Style id=\"cluster1\"><IconStyle>");
		print.println("  <color>ff999999</color>");
		print.println("  <Icon><href>http://www.gstatic.com/mapspro/images/stock/503-wht-blank_maps.png</href></Icon>");
		print.println("</IconStyle></Style>");
		print.println("<Style id=\"cluster2\"><IconStyle>");
		print.println("  <color>ff0000ff</color>");
		print.println("  <Icon><href>http://www.gstatic.com/mapspro/images/stock/503-wht-blank_maps.png</href></Icon>");
		print.println("</IconStyle></Style>");
		print.println("<Style id=\"cluster3\"><IconStyle>");
		print.println("  <color>ff0099ff</color>");
		print.println("  <Icon><href>http://www.gstatic.com/mapspro/images/stock/503-wht-blank_maps.png</href></Icon>");
		print.println("</IconStyle></Style>");
		print.println("<Style id=\"cluster4\"><IconStyle>");
		print.println("  <color>ff00ffff</color>");
		print.println("  <Icon><href>http://www.gstatic.com/mapspro/images/stock/503-wht-blank_maps.png</href></Icon>");
		print.println("</IconStyle></Style>");
		
		for (int i = 0; i < places.size(); i++) {
			print.println("<Placemark>");
			print.println("  <name>" + places.get(i).getName() + "</name>");
			print.println("  <description>" + places.get(i).getDescription() + "</description>");
			print.println("  <styleUrl>#cluster" + places.get(i).getClusterID() + "</styleUrl>");
			print.printf("  <Point><coordinates>%f,%f</coordinates></Point>%n", places.get(i).getLocation().getLongitude(), places.get(i).getLocation().getLatitude());
			print.println("</Placemark>");
		}
		
		print.println("</Document>");
		print.println("</kml>");
		print.close();
	}
}