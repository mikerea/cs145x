package hw7;

import java.util.ArrayList;

public class KMeans {
	public static int indexOfNearestMean(LatLon[] means, LatLon loc){
		
		double minDistance = Double.MAX_VALUE;
		int index = 0;
		
		for(int i = 0; i < means.length; i++){
			if(loc.distanceTo(means[i]) < minDistance){
				minDistance = loc.distanceTo(means[i]);
				index = i;
			}
		}
		
		return index;
	}
	
	public static boolean updateMembershipAndMeans(LatLon[] means, ArrayList<Place> places){
		
		boolean updated = false;
		ArrayList<Cluster> clus = new ArrayList<Cluster>();
		
		for(int i = 0; i < places.size(); i++){
			clus.add(new Cluster());
		}
		
		for(int i = 0; i < places.size(); i++){
			int ID = indexOfNearestMean(means, places.get(i).getLocation());
			if (ID != places.get(i).getClusterID()) {
				updated = true;
				places.get(i).setClusterID(ID);
			}
			clus.get(ID).add(places.get(i).getLocation());
		}
		
		for (int a = 0; a < means.length; a++) {
			LatLon newMeans = clus.get(a).getMeanLocation();
			means[a] = newMeans;
		}
		
		return updated;
	}
	
	public static void cluster(ArrayList<Place> places, int k){
		LatLon[] ll = new LatLon[k];
		
		for(int i = 0; i < k; i++){
			ll[i] = places.get(i).getLocation();
		}
		
		while(updateMembershipAndMeans(ll, places)){
			//Repeat the above boolean test until there are no new changes.
		}
	}
}
