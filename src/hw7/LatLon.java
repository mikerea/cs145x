package hw7;

public class LatLon {

	private double longitude;
	private double latitude;
	
	public LatLon(double lat, double lon){
		this.longitude = lon;
		this.latitude = lat;
	}
	
	public double getLongitude(){
		return longitude;
	}
	
	public double getLatitude(){
		return latitude;
	}
	
	public double distanceTo(LatLon ll){
		return Math.sqrt(Math.pow(ll.getLongitude() - this.longitude, 2) + Math.pow(ll.getLatitude() - this.latitude, 2));
	}
}
