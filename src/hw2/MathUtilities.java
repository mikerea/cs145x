package hw2;

public class MathUtilities {
	
	public static void main(String arg[]){
		System.out.println(lerp(3,-1,11,10,7));		
	}
	
	static public double getHypotenuseDifference(double a, double b){
		double dif = a + b - Math.sqrt(a*a + b*b);
		return dif;
	}
	
	static public double lerp(double x1, double y1, double x2, double y2, double x3){
		double y3 = 0.0;
		//This jumbled line of code is the process of solving the equation given in the homework.
		//I double checked the math with my own work and WolframAlpha.
		y3 = ((((x2 - x1)/(y2-y1)) * y1) + (x3-x1))/((x2-x1)/(y2-y1));
		return y3;
	}
}
