package hw2;

import java.util.Scanner;

public class StringUtilities {
	
	//for testing purposes
	public static void main(String arg[]){
		Scanner scanner = new Scanner(System.in);
        String ename, elist;
        System.out.println("Enter Employee Name:");
        ename=scanner.nextLine();
        System.out.println(extractBracketed(ename));
        
        System.out.println("Enter Employee list:");
        elist=scanner.nextLine();
        System.out.println(getThird(elist));
	}

	static public String extractBracketed(String sent){
		//Starts right after the left bracket, ends right before the right bracket.
		return sent.substring(sent.indexOf('[') + 1,sent.indexOf(']'));
	}
	
	static public String getThird(String list){

		//Removes ALL types of whitespace.
		list = list.replaceAll("\\p{Z}","");
		
		for(int i = 0; i < 2; i++){
			list = list.substring(list.indexOf(',') + 1, list.length());
		}
		
		//This is for determining if there are one or more items in the list.
		int counter = 0;
		for(int i=0; i<list.length(); i++){
		    if(list.charAt(i) == ','){
		        counter++;
		    } 
		}
		
		//If there's one comma, that needs to be removed. Else nothing happens.
		if(counter == 1){
			list = list.substring(0, list.indexOf(','));
		}
		else{
		}
		
		return list;
	}
}
