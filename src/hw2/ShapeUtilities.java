package hw2;

public class ShapeUtilities {
	static public double getCircleArea(double r){
		r = Math.PI * (r * r);
		return r;
	}
	
	static public double getAnnulusArea(double outR, double inR){
		double area = (Math.PI * (outR * outR)) - (Math.PI * (inR * inR));
		return area;
	}
}
