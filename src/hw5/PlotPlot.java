package hw5;

import java.util.ArrayList;

public class PlotPlot {
	//Could've used String.split, but this works and efficiency is not currently the goal.
	public static int[] getChoicesForOnePage(String page){
		int[] next;
		if(page.equals("")){ //If there's no pages, return blank.
			next = new int[0];
		}
		else if(page.equals("p")){ //If it's an end, return null.
			return null;
		}
		else{
			next = new int[page.length() - page.replace(",", "").length() + 1]; //Finds the number of commas by using the difference of with vs. without commas. 
																				//+1 because the number of commas is always one less than the number of pages.
			for(int i = 0; i < next.length; i++){
				if(page.indexOf(",") != -1){ //If there's still commas.
					next[i] = Integer.parseInt(page.substring(0, page.indexOf(","))) - 1; //Add the first number.
					page = page.substring(page.indexOf(",") + 1); //Remove the first number and the first comma.
				}
				else{
					next[i] = Integer.parseInt(page) - 1; //Add the final number.
				}
			}
		}
		return next;
	}
	
	//Could've used String.split here too...
	public static int[][] getChoicesForAllPages(String page){
		int[][] next = new int[page.length() - page.replace("|", "").length() + 1][]; //Same idea as before.
		for(int i = 0; i < next.length; i++){
			if(page.indexOf("|") != -1){ //If there's still verticals.
				next[i] = getChoicesForOnePage(page.substring(0, page.indexOf("|"))); //Add the first number.
				page = page.substring(page.indexOf("|") + 1); //Remove the first number and the first vertical.
			}
			else{
				next[i] = getChoicesForOnePage(page); //Add the final number.
			}
		}
		return next;
	}
	
	public static ArrayList<Integer> getForkPages(int[][] pages){
		ArrayList<Integer> pageList = new ArrayList<Integer>();
		for(int i = 0; i < pages.length; i++){
			try{
				if(pages[i].length > 1){ //If it has more than one element, it has multiple pages.
					pageList.add(i);
				}
			}
			catch(NullPointerException e){ //Do nothing if it's a picture.
				
			}
		}
		return pageList;
	}
	
	public static ArrayList<Integer> getEndPages(int[][] pages){
		ArrayList<Integer> pageList = new ArrayList<Integer>();
		for(int i = 0; i < pages.length; i++){
			try{
				if(pages[i].length == 0){
					pageList.add(i);
				}
			}
			catch(NullPointerException e){ //Do nothing if it's a picture.
				
			}
		}
		return pageList;
	}
	
	public static ArrayList<Integer> getOrphanPages(int[][] pages){
		ArrayList<Integer> pageList = new ArrayList<Integer>();
		boolean[] hasPrevious = new boolean[pages.length]; //Keeps track of what has previous pages.
		for(int i = 0; i < pages.length; i++){
			try{
				for(int j = 0; j < pages[i].length; j++){
					try{
						hasPrevious[pages[i][j]] = true;
					}
					catch(IndexOutOfBoundsException e){
						
					}
				}
			}
			catch(NullPointerException e){
				
			}
		}
		for(int i = 1; i < hasPrevious.length; i++){
			if(hasPrevious[i] == false && pages[i] != null){ //If it doesn't have a previous page, add it to the lsit.
				pageList.add(i);
			}
		}
		return pageList;
	}
	
	public static void dotEndPages(int[][] pages){ //Get things from getEndPages, print them out.
		ArrayList<Integer> list = getEndPages(pages);
		for(int i = 0; i < list.size(); i++){
			System.out.println("  " + (list.get(i) + 1) + " [peripheries=2];");
		}
	}
	
	public static void dotOrphanPages(int[][] pages){ //Get things from getOrphanPages, print them out.
		ArrayList<Integer> list = getOrphanPages(pages);
		for(int i = 0; i < list.size(); i++){
			System.out.println("  " + (list.get(i) + 1) + " [style=dashed];");
		}
	}
	
	public static void dotPlot(int[][] pages){ //Print out every page that leads to another and the number(s) they lead to.
		for(int i = 0; i < pages.length; i++){
			try{
				for(int j = 0; j < pages[i].length; j++){
					System.out.println("  " + (i + 1) + " -> " + (pages[i][j] + 1) + ";");
				}
			}
			catch(NullPointerException e){
				
			}
		}
	}
	
	public static void dot(int[][] pages){ //Pretty self-explanatory.
		System.out.println("digraph G {");
		dotEndPages(pages);
		dotOrphanPages(pages);
		dotPlot(pages);
		System.out.println("}");
	}
	
	//This doesn't feel like a particularly well-written code... It seems like there's some redundancy that could be removed.
	public static ArrayList<Integer> getPathOfFirsts(int[][] pages){
		ArrayList<Integer> list = new ArrayList<Integer>();
		int page = 0;
		list.add(page);
		boolean foundEnd = false;
		while(!foundEnd){
			try{
				list.add(pages[page][0]);
				page = pages[page][0];
			}
			catch(ArrayIndexOutOfBoundsException e){
				foundEnd = true;
			}
		}
		return list;
	}
}
