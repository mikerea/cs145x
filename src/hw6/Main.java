package hw6;

import java.io.File;
import java.io.IOException;

import javax.swing.JFileChooser;

public class Main {
	public static void main(String args[]){
		int count = 0;
		int maxRetry = 5;
		
		while(maxRetry == 5){ //A simple way to allow for retrying with a try-catch.
			try{
				JFileChooser chooser = new JFileChooser();
				chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				int result = chooser.showOpenDialog(null);
		
				if(result == JFileChooser.APPROVE_OPTION){
					File selectedDir = chooser.getSelectedFile();
					Playright.compressAndShow(selectedDir);
				}
				break;
			}
			catch(IOException e){
				if(++count == maxRetry){
					System.exit(0);
				}
			}
		}
	}
}
