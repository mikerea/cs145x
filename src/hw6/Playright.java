package hw6;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import javax.imageio.ImageIO;

public class Playright {
	public static File[] getSortedContents(File f){
		File[] list = f.listFiles();
		
		Arrays.sort(list);
		
		//Very, very obnoxious glitch wherein the filereader was taking in my four .pngs along with one thumbnails.db file. Had to remove it somehow.
		String tempString = list[list.length-1].getName();
		if(tempString.substring(tempString.length() - 2).equals("db")){
			File[] temp = new File[list.length - 1];
			for(int j = 0; j < temp.length; j++){
				temp[j] = list[j];
			}
			list = new File[temp.length];
			list = temp;
		}
		
		return list;
	}
	
	public static BufferedImage[] readImages(File[] l) throws IOException{
		BufferedImage[] buf = new BufferedImage[l.length];
		
		for(int i = 0; i < buf.length; i++){
			buf[i] = ImageIO.read(l[i]);
		}
		
		return buf;
	}
	
	public static BufferedImage compress(BufferedImage[] b){
		BufferedImage im = null;
		try{
			im = new BufferedImage(b[0].getWidth(), b[0].getHeight(), b[0].getType());
				int k = 0; //k is the image being selected. Resets to 0 when it hits the last image
				for(int i = 0; i < b[0].getWidth(); i++){ //i is the x value
					for(int j = 0; j < b[0].getHeight(); j++){ //j is the y value
						im.setRGB(i, j, b[k].getRGB(i, j));
					}
					k++;
					if(k == b.length){
						k = 0;
					}
				}
		}
		catch(IllegalArgumentException e){
			System.out.println("There are no images in this folder.");
			System.exit(0);
		}	
		
		return im;
	}
	
	public static void compressAndShow(File f) throws IOException{
		@SuppressWarnings("unused") //Glorious, warning-free code.
		PlayrightViewer play = new PlayrightViewer(compress(readImages(getSortedContents(f))), f.listFiles().length - 1);
	}
}
