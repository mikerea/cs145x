package hw3;

public class Main {
	public static void main(String[] args) {
		System.out.println("inBox " + Trutilities.inBox(0, 0, 10, 10, 5, 5));
		System.out.println("inCircle " + Trutilities.inCircle(0, 0, 10, 0, 9));
		System.out.println("isGrayscale " + Trutilities.isGrayscale("#555555"));
		System.out.println("isNorth " + Trutilities.isNorth(60, "50�40\'30\""));
		System.out.println("isBefore " + Trutilities.isBefore(2006, 3, 20, 2011, 11, 11));
		System.out.println("isEqualEnough " + Trutilities.isEqualEnough(0, 0, 5));
		System.out.println("isPowerOfTen " + Trutilities.isPowerOfTen(1000));
		System.out.println("isGameOver " + Trutilities.isGameOver("dOnE"));
	}
}
