package hw3;

public class Trutilities {

	public static boolean inBox(double x1, double y1, double x2, double y2, double x3, double y3){
		
		boolean inside = false;
		
		if(x3 >= x1){
			if(y3 >= y1){
				if(x3 <= x2){
					if(y3 <= y2){
						inside = true;
					}
				}
			}
		}
		
		return inside;
	}
	
	public static boolean inCircle(double x1, double y1, double r, double x2, double y2){
		
		boolean inside = false;
		double dist = Math.sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1)); //Distance equation
		if(dist <= r){
			inside = true;
		}
		return inside;
	}
	
	public static boolean isGrayscale(String hex){
		boolean isGray = false;
		hex = hex.substring(1); //Removes hashtag
		
		for(int i = 0; i < hex.length(); i++){
			if(hex.charAt(0) == hex.charAt(i)){
				isGray = true;
			}
			else{
				isGray = false;
				i = hex.length() - 1; //Ends For loop
			}
		}
		
		return isGray;		
	}
	
	public static boolean isNorth(double lat, String dms){
		boolean isNorth = false;
		
		//Make the String parts into Doubles. Then, add the smaller number (divided by sixty to be mathematically equivalent).
		double d = Double.parseDouble(dms.substring(0, dms.indexOf('\u00B0')));
		double m = Double.parseDouble(dms.substring(dms.indexOf('\u00B0') + 1, dms.indexOf('\'')))/60;
		double s = Double.parseDouble(dms.substring(dms.indexOf('\'') + 1, dms.indexOf('\"')))/60/60;
		
		if(lat > (d + m + s)){
			isNorth = true;
		}
				
		return isNorth;
	}
	
	public static boolean isBefore(int y1, int m1, int d1, int y2, int m2, int d2){
		boolean isBefore = false;
		
		//Checks years first because that would be the most obvious, then months, then days. Each is only checked if necessary.
		if(y1 < y2){
			isBefore = true;
		}
		else if(y1 == y2){
			if(m1 < m2){
				isBefore = true;
			}
			else if(m1 == m2){
				if(d1 < d2){
					isBefore = true;
				}
			}
		}
		
		return isBefore;		
	}
	
	public static boolean isEqualEnough(double a, double b, double threshold){
		boolean isEnough = false;
		
		//Seems self-explanatory.
		if((a - threshold <= b && b < a) || (a + threshold >= b && b > a) || a == b){
			isEnough = true;
		}
		
		return isEnough;
	}
	
	public static boolean isPowerOfTen(int num){
		boolean isPower = false;
		
		//Math!
		if(Math.log10(num) == Math.floor(Math.log10(num))){
			isPower = true;
		}
		
		return isPower;
	}
	
	public static boolean isGameOver(String command){
		boolean isOver = false;
		
		if(command.toLowerCase().equals("quit") || command.toLowerCase().equals("exit") || command.toLowerCase().equals("done")){
			isOver = true;
		}
		
		return isOver;
	}
}
