package hw4;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.IIOException;
import javax.imageio.ImageIO;

public class Main {
	public static void main(String[] args) throws IOException{
		try{
			BufferedImage pre = ImageIO.read(new File(System.getProperty("user.dir") + "/src/hw4/Images/pre.jpg"));
			BufferedImage post = ImageIO.read(new File(System.getProperty("user.dir") + "/src/hw4/Images/post.jpg"));
			new SplittinImage(pre, post, TwixPix.DISSOLVE);
		}
		catch(IIOException e){
			System.out.println("There's no file there.");
		}
	}
}
