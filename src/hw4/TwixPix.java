package hw4;

import java.awt.Color;
import java.awt.image.BufferedImage;

public class TwixPix {
	public static final int ROLL_UP = 4, WIPE_LEFT = 3, CROSSFADE = 1, DISSOLVE = 0, CLOCK = 5, BOX_OUT = 2;

	public static BufferedImage dissolve(BufferedImage from, BufferedImage to, double proportion){
		BufferedImage newImg = new BufferedImage(from.getWidth(), from.getHeight(), from.getType());
		
		for(int i = 0; i < newImg.getWidth(); i++){
			for(int j = 0; j < newImg.getHeight(); j++){
				Color cOne = new Color(from.getRGB(i, j));
				Color cTwo = new Color(to.getRGB(i, j));
				Color newColor = new Color((int)((1 - proportion) * cOne.getRed() + proportion * cTwo.getRed()), (int)((1 - proportion) * cOne.getGreen() + proportion * cTwo.getGreen()), (int)((1 - proportion) * cOne.getBlue() + proportion * cTwo.getBlue()));
				newImg.setRGB(i, j, newColor.getRGB());
			}
		}
		
		return newImg;
	}
	
	public static BufferedImage crossfade(BufferedImage from, BufferedImage to, double proportion){
		BufferedImage newImg = new BufferedImage(from.getWidth(), from.getHeight(), from.getType());
		
		for(int i = 0; i < newImg.getWidth(); i++){
			for(int j = 0; j < newImg.getHeight(); j++){
				if(proportion < 0.5){
					double tempPro = proportion * 2;
					Color cOne = new Color(from.getRGB(i, j));
					Color newColor = new Color((int)((1 - tempPro) * cOne.getRed()), (int)((1 - tempPro) * cOne.getGreen()), (int)((1 - tempPro) * cOne.getBlue()));
					newImg.setRGB(i, j, newColor.getRGB());
				}
				else if(proportion > 0.5){
					double tempPro = (proportion - 0.5) * 2;
					Color cOne = new Color(to.getRGB(i, j));
					Color newColor = new Color((int)(tempPro * cOne.getRed()), (int)(tempPro * cOne.getGreen()), (int)(tempPro * cOne.getBlue()));
					newImg.setRGB(i, j, newColor.getRGB());
				}
				else{ //In the event of 0.5
					Color newColor = new Color(0, 0, 0);
					newImg.setRGB(i, j, newColor.getRGB());
				}
			}
		}
		
		return newImg;
	}

	public static BufferedImage boxOut(BufferedImage from, BufferedImage to, double proportion){
		BufferedImage newImg = new BufferedImage(from.getWidth(), from.getHeight(), from.getType());
		
		for(int i = 0; i < newImg.getWidth(); i++){
			for(int j = 0; j < newImg.getHeight(); j++){
				if(i < newImg.getWidth()/2 - (newImg.getWidth() * proportion/2) || i > newImg.getWidth()/2 + newImg.getWidth()/2 * proportion - 1
						|| j < newImg.getHeight()/2 - (newImg.getHeight() * proportion/2) || j > newImg.getHeight()/2 + newImg.getHeight()/2 * proportion){
					newImg.setRGB(i, j, from.getRGB(i, j));
				}
				else{
					newImg.setRGB(i, j, to.getRGB(i, j));
				}
			}
		}
		
		return newImg;
	}

	public static BufferedImage wipeLeft(BufferedImage from, BufferedImage to, double proportion){
		BufferedImage newImg = new BufferedImage(from.getWidth(), from.getHeight(), from.getType());
		
		for(int i = 0; i < newImg.getWidth(); i++){
			for(int j = 0; j < newImg.getHeight(); j++){
				if(i < newImg.getWidth() - (newImg.getWidth() * proportion)){ //If the x coordinate is left of the transition.
					newImg.setRGB(i, j, from.getRGB(i, j));
				}
				else{
					newImg.setRGB(i, j, to.getRGB(i, j));
				}
			}
		}
		
		return newImg;
	}

	public static BufferedImage rollUp(BufferedImage from, BufferedImage to, double proportion){
		BufferedImage newImg = new BufferedImage(from.getWidth(), from.getHeight(), from.getType());
		
		for(int i = 0; i < newImg.getWidth(); i++){
			for(int j = 0; j < newImg.getHeight(); j++){
				if(j < newImg.getHeight() - (newImg.getHeight() * proportion)){
					try{
						newImg.setRGB(i, j, from.getRGB(i, (int)(j + newImg.getHeight() * proportion)));
					}
					catch(ArrayIndexOutOfBoundsException e){
						newImg.setRGB(i, j, from.getRGB(i, (int)(j + newImg.getHeight() * proportion) - 1)); //Exactly ONE scenario caused an exception: 0.7 proportion with the second set of pictures. The math didn't work out. So I forced my way around it.
					}
				}
				else{
					newImg.setRGB(i, j, to.getRGB(i, j - (int)(to.getHeight() * (1 - proportion))));
				}
			}
		}
		
		return newImg;
	}

	public static BufferedImage clock(BufferedImage from, BufferedImage to, double proportion){
		BufferedImage newImg = new BufferedImage(from.getWidth(), from.getHeight(), from.getType());
		
		for(int i = 0; i < newImg.getWidth(); i++){
			for(int j = 0; j < newImg.getHeight(); j++){
				double theta = Math.atan2((j - newImg.getHeight()/2), (i - newImg.getWidth()/2)) + Math.PI/2;
				if(theta < 0){
					theta += 2*Math.PI;
				}
				theta /= 2*Math.PI;
				if(theta >= proportion){
					newImg.setRGB(i, j, from.getRGB(i, j));
				}
				else{
					newImg.setRGB(i, j, to.getRGB(i, j));
				}
			}
		}
		
		return newImg;
	}

	public static BufferedImage transition(BufferedImage from, BufferedImage to, double proportion, int trans){
		BufferedImage ret = new BufferedImage(from.getWidth(), to.getHeight(), from.getType());
		
		switch(trans){
		case 0: ret = dissolve(from, to, proportion);
			break;
		case 1: ret = crossfade(from, to, proportion);
			break;
		case 2: ret = boxOut(from, to, proportion);
			break;
		case 3: ret = wipeLeft(from, to, proportion);
			break;
		case 4: ret = rollUp(from, to, proportion);
			break;
		case 5: ret = clock(from, to, proportion);
			break;
		}
		
		return ret;
	}
}
