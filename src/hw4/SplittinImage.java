package hw4;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class SplittinImage extends JFrame implements ChangeListener {
  private BufferedImage pre;
  private BufferedImage post;
  private JLabel label;
  private JSlider slider;
  private JComboBox<Transition> transitionBox;

  private static final Transition[] transitions = {
    new Transition("Dissolve", TwixPix.DISSOLVE),
    new Transition("Crossfade", TwixPix.CROSSFADE),
    new Transition("Wipe Left", TwixPix.WIPE_LEFT),
    new Transition("Roll Up", TwixPix.ROLL_UP),
    new Transition("Box Out", TwixPix.BOX_OUT),
    new Transition("Clock", TwixPix.CLOCK),
  };

  public SplittinImage(BufferedImage pre,
                       BufferedImage post,
                       int transitionType) {
    this.pre = pre;
    this.post = post;

    if (pre.getWidth() != post.getWidth() ||
        pre.getHeight() != post.getHeight()) {
      throw new IllegalArgumentException("Images must be of the same size");
    }

    JPanel toolbar = new JPanel(new BorderLayout());

    slider = new JSlider(0, 1000, 0);
    slider.addChangeListener(this);
    toolbar.add(slider);

    transitionBox = new JComboBox<>(transitions);
    transitionBox.setSelectedItem(find(transitionType));
    toolbar.add(transitionBox, BorderLayout.EAST);

    transitionBox.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        slider.setValue(0);
        updateImage();
      }
    });

    add(toolbar, BorderLayout.NORTH);

    label = new JLabel(new ImageIcon(pre));
    updateImage();
    add(label, BorderLayout.CENTER);

    pack();
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    setVisible(true);
  }

  @Override
  public void stateChanged(ChangeEvent event) {
    updateImage();
  }

  private void updateImage() {
    Transition x = (Transition) transitionBox.getSelectedItem();
    label.setIcon(new ImageIcon(TwixPix.transition(pre, post, slider.getValue() / 1000.0, x.getID())));
  }

  private static Transition find(int transitionType) {
    for (Transition x : transitions) {
      if (x.getID() == transitionType) {
        return x;
      }
    }
    return null;
  }

  private static class Transition {
    private String name;
    private int id;

    public Transition(String name,
                      int id) {
      this.name = name;
      this.id = id;
    }

    public String toString() {
      return name;
    }

    public int getID() {
      return id;
    }
  }
}
